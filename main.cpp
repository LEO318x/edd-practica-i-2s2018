#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include <ctype.h>
#include <string.h>
#include <fstream>

using namespace std;
void colorT(string texto, int i);
void limpiar();
void esperar();

typedef struct nodoPila{
    int id;
    char valor;
    struct nodoPila* siguiente;
}nodoPila;

typedef struct nodoCola{
    int id;
    char valor;
    struct nodoCola* siguiente;
}nodoCola;

typedef struct nodoListaDoble{
    int id;
    char valor;
    struct nodoListaDoble* siguiente;
    struct nodoListaDoble* atras;
}nodoListaDoble;

typedef struct nodoListaCircular{
    int id;
    char valor;
    nodoListaCircular* siguiente;
}nodoListaCircular;

typedef struct nodoFila{
    int id;
    struct nodoFila *siguiente;
	struct nodoColumna *columna;
} *pListaFila;

typedef struct nodoColumna{
    int id;
	int valor;
    struct nodoColumna *siguiente;
} *pListaColumna;

typedef struct nodoPilaDeshacer{
    int iFila;
    int iColumna;
    int valor;
    struct nodoPilaDeshacer *siguiente;
} *pPilaDeshacer;

typedef struct nodoPilaRepetir{
    int iFila;
    int iColumna;
    int valor;
    struct nodoPilaRepetir *siguiente;
} *pPilaRepetir;

void insertarPilaDeshacer(pPilaDeshacer &lista, int iFila, int iColumna, int valor){
    pPilaDeshacer nuevo = new (struct nodoPilaDeshacer);
    pPilaDeshacer auxLista = new (struct nodoPilaDeshacer);


    nuevo->iFila = iFila;
    nuevo->iColumna = iColumna;
    nuevo->valor = valor;

    if(lista == NULL){
       lista = nuevo;
    }else{
       auxLista = lista;
       nuevo->siguiente = auxLista;
       auxLista = nuevo;
       lista = auxLista;
    }
}

void insertarPilaRepetir(pPilaRepetir &lista, int iFila, int iColumna, int valor){
    pPilaRepetir nuevo = new (struct nodoPilaRepetir);
    pPilaRepetir auxLista = new (struct nodoPilaRepetir);


    nuevo->iFila = iFila;
    nuevo->iColumna = iColumna;
    nuevo->valor = valor;

    if(lista == NULL){
       lista = nuevo;
    }else{
       auxLista = lista;
       nuevo->siguiente = auxLista;
       auxLista = nuevo;
       lista = auxLista;
    }
}

void desplegarPilaRepetir(pPilaRepetir lista){
    pPilaRepetir actual = new (struct nodoPilaRepetir);
    actual = lista;
    if(actual!=NULL){
        while(actual !=NULL){
            cout << "iFila: " << actual->iFila << " iColumna: " << actual->iColumna << " Valor: " << actual->valor << endl;
            actual = actual->siguiente;
        }
    }else{
        cout << "La pila repetir esta vacia " << endl;
    }
}

void desplegarPilaDeshacer(pPilaDeshacer lista){
    pPilaDeshacer actual = new (struct nodoPilaDeshacer);
    actual = lista;
    if(actual!=NULL){
        while(actual !=NULL){
            cout << "iFila: " << actual->iFila << " iColumna: " << actual->iColumna << " Valor: " << actual->valor << endl;
            actual = actual->siguiente;
        }
    }else{
        cout << "La pila deshacer esta vacia " << endl;
    }
}

void insertarC(pListaColumna &lista, int valor){
    pListaColumna nuevo = new (struct nodoColumna);
    pListaColumna aux = new (struct nodoColumna);

    nuevo->id = valor;
    nuevo->valor = 0;
    nuevo->siguiente = NULL;

    if(lista == NULL){
        lista = nuevo;
    }else{
        aux = lista;
        while(aux->siguiente!=NULL){
            aux = aux->siguiente;
        }
        aux->siguiente = nuevo;
    }
}

void insertarF(pListaFila &lista, int valor, int iC){
    pListaFila nuevo = new (struct nodoFila);
    pListaFila aux = new (struct nodoFila);
    pListaColumna columna = NULL;
    for(int i = 0; i < iC; i++){
        insertarC(columna, i);
    }

    nuevo->id = valor;
    nuevo->columna = columna;
    nuevo->siguiente = NULL;

    if(lista == NULL){
        lista = nuevo;
    }else{
        aux = lista;
        while(aux->siguiente!=NULL){
            aux = aux->siguiente;
        }
        aux->siguiente = nuevo;
    }
}

void desplegarF(pListaFila lista){
    while(lista != NULL){
        cout << "Fila: " << lista->id << endl;
        pListaColumna aux = lista->columna;
        while(aux != NULL){
            cout << "Columna: " << aux->id << " Valor: " << aux->valor << endl;
            aux = aux->siguiente;
        }
        lista = lista->siguiente;
    }

}

int buscar(pListaFila lista, int iFila, int iColumna){
    int dato = 0;
    while(lista != NULL ){
        if(iFila == lista->id){
            pListaColumna aux = lista->columna;
            while(aux != NULL){
                if(iColumna == aux->id){
                    //cout << "La coordenada contiene el valor: " << aux->valor << endl;
                    dato = aux->valor;
                }
                aux = aux->siguiente;
            }
        }
        lista = lista->siguiente;
    }
    return dato;
}

void modificar(pListaFila lista, int iFila, int iColumna, int valor){
    while(lista != NULL ){
            if(iFila == lista->id){
                pListaColumna aux = lista->columna;
                while(aux != NULL){
                    if(iColumna == aux->id){
                        aux->valor = valor;
                        cout << "Se ha modificado la coordenada" << endl;
                    }
                    aux = aux->siguiente;
                }
            }
            lista = lista->siguiente;
        }
}

void eliminarPilaDeshacer(pPilaDeshacer &lista, pPilaRepetir &listaRepetir, pListaFila listaFila){
    pPilaDeshacer actual = new (struct nodoPilaDeshacer);
    pPilaDeshacer anterior = new (struct nodoPilaDeshacer);
    anterior = NULL;
    actual = lista;
    if(lista!=NULL){
         if(actual == lista){
            insertarPilaRepetir(listaRepetir, lista->iFila, lista->iColumna, lista->valor);
            modificar(listaFila, actual->iFila, actual->iColumna, 0);
            lista = lista -> siguiente;
         }else{
            anterior -> siguiente = actual->siguiente;
         }
         cout << "Nodo Eliminado" << endl;
    }else{
        cout << "La pila deshacer esta vacia " << endl;
    }
}

void eliminarPilaRepetir(pPilaRepetir &lista, pPilaDeshacer &listaDeshacer, pListaFila listaFila){
    pPilaRepetir actual = new (struct nodoPilaRepetir);
    pPilaRepetir anterior = new (struct nodoPilaRepetir);
    anterior = NULL;
    actual = lista;
    if(lista!=NULL){
         if(actual == lista){
            insertarPilaDeshacer(listaDeshacer, actual->iFila, actual->iColumna, actual->valor);
            modificar(listaFila, actual->iFila, actual->iColumna, actual->valor);
            lista = lista -> siguiente;
         }else{
            anterior -> siguiente = actual->siguiente;
         }
         cout << "Nodo Eliminado" << endl;
    }else{
        cout << "La pila repetir esta vacia " << endl;
    }
}

void crearHoja(pListaColumna &listaColumna, pListaFila &listaFila, int iFila, int iColumna){

    for(int i = 0; i < iFila; i++){
        insertarF(listaFila, i, iColumna);
    }
    //desplegarF(listaFila);
    cout << "Hoja creada" << endl;
}

nodoPila* primeroP = NULL;

nodoCola* primeroC = NULL;
nodoCola* ultimoC = NULL;

nodoListaDoble* primerold = NULL;
nodoListaDoble* ultimold = NULL;

nodoListaCircular* primerolc = NULL;
nodoListaCircular* ultimolc = NULL;

void insertarNodoPila();
void buscarNodoPila();
void desplegarPila();
void modificarNodoPila();
void eliminarNodoPila();
void graficarPila();

void insertarNodoCola();
void desplegarCola();
void buscarNodoCola();
void modificarNodoCola();
void eliminarNodoCola();
void graficarCola();

void insertarNodoListaDoble();
void insertarNodoListaDobleOrdenada();
void desplegarNodoListaDoble();
void buscarNodoListaDoble();
void modificarNodoListaDoble();
void eliminarNodoListaDoble();
void graficarListaDoble();

void insertarNodoListaCircular();
void insertarNodoListaCircularOrdenada();
void desplegarNodoListaCircular();
void buscarNodoListaCircular();
void modificarNodoListaCircular();
void eliminarNodoListaCircular();
void graficarListaCircular();

void graficarHojaCalculo(pListaFila &lista);

int validarNum();
int opcionMenu;

pListaColumna listaColumna = NULL;
pListaFila listaFila = NULL;
pPilaDeshacer pilaDeshacer = NULL;
pPilaRepetir pilaRepetir = NULL;

void crearArchivo();
int main() {

    retornoMenuPrincipal:
    colorT("##########################################", 10);
    colorT("##### [EDD-B] Practica 1 - 201212535 #####", 10);
    colorT("##########################################", 10);
    colorT("Escoge una opción: ", 11);
    colorT("1. Estructura de Datos", 14);
    colorT("2. Hoja de Calculo", 14);
    colorT("9. Salir", 12);
    opcionMenu = validarNum();
    limpiar();
    switch(opcionMenu){
        case 1:
            retornoEstructuraDatos:
            colorT("##################################", 10);
            colorT("##### 1. ESTRUCTURA DE DATOS #####", 10);
            colorT("##################################", 10);
            colorT("Escoge una opcion:", 11);
            colorT("1. Pila", 14);
            colorT("2. Cola", 14);
            colorT("3. Lista doblemente enlazada", 14);
            colorT("4. Lista circular simplemente enlazada", 14);
            colorT("0. Regresar", 15);
            opcionMenu = validarNum();;
            limpiar();
            switch (opcionMenu){
                case 1:
                    retornoMenuPila:
                    colorT("##################################", 10);
                    colorT("####         1.1  PILA        ####", 10);
                    colorT("##################################", 10);
                    colorT("Escoge una opción: ", 11);
                    colorT("1. Insertar", 14);
                    colorT("2. Ver", 14);
                    colorT("3. Editar", 14);
                    colorT("4. Eliminar", 14);
                    colorT("5. Graficar", 14);
                    colorT("6. Desplegar", 14);
                    colorT("0. Regresar", 15);
                    opcionMenu = validarNum();;
                    limpiar();
                    switch(opcionMenu){
                        case 0:
                            limpiar();
                            goto retornoEstructuraDatos;
                            break;
                        case 1:
                            limpiar();
                            insertarNodoPila();
                            goto retornoMenuPila;
                            break;
                        case 2:
                            limpiar();
                            buscarNodoPila();
                            goto retornoMenuPila;
                            break;
                        case 3:
                            limpiar();
                            modificarNodoPila();
                            goto retornoMenuPila;
                            break;
                        case 4:
                            limpiar();
                            eliminarNodoPila();
                            goto retornoMenuPila;
                            break;
                        case 5:
                            //Graficar
                            limpiar();
                            graficarPila();
                            goto retornoMenuPila;
                            break;
                        case 6:
                            limpiar();
                            desplegarPila();
                            goto retornoMenuPila;
                            break;
                        default:
                            cout << "Opción no válida, intenta de nuevo" << endl;
                            esperar();
                            limpiar();
                            goto retornoMenuPila;
                    }
                    break;
                case 2:
                    retornoMenuCola:
                    colorT("##################################", 10);
                    colorT("####         1.1  COLA        ####", 10);
                    colorT("##################################", 10);
                    colorT("Escoge una opción: ", 11);
                    colorT("1. Insertar", 14);
                    colorT("2. Ver", 14);
                    colorT("3. Editar", 14);
                    colorT("4. Eliminar", 14);
                    colorT("5. Graficar", 14);
                    colorT("6. Desplegar", 14);
                    colorT("0. Regresar", 15);
                    opcionMenu = validarNum();;
                    limpiar();
                    switch(opcionMenu){
                        case 0:
                            limpiar();
                            goto retornoEstructuraDatos;
                            break;
                        case 1:
                            limpiar();
                            insertarNodoCola();
                            goto retornoMenuCola;
                            break;
                        case 2:
                            limpiar();
                            buscarNodoCola();
                            goto retornoMenuCola;
                            break;
                        case 3:
                            limpiar();
                            modificarNodoCola();
                            goto retornoMenuCola;
                            break;
                        case 4:
                            limpiar();
                            eliminarNodoCola();
                            goto retornoMenuCola;
                            break;
                        case 5:
                            //Graficar
                            limpiar();
                            graficarCola();
                            goto retornoMenuCola;
                            break;
                        case 6:
                            limpiar();
                            desplegarCola();
                            goto retornoMenuCola;
                            break;
                        default:
                            cout << "Opción no válida, intenta de nuevo" << endl;
                            goto retornoMenuCola;
                    }
                    break;
                case 3:
                retornoMenuListaDoble:
                    colorT("#####################################################", 10);
                    colorT("####         1.3 LISTA DOBLEMENTE ENLAZADA       ####", 10);
                    colorT("#####################################################", 10);
                    colorT("Escoge una opción: ", 11);
                    colorT("1. Insertar", 14);
                    colorT("2. Ver", 14);
                    colorT("3. Editar", 14);
                    colorT("4. Eliminar", 14);
                    colorT("5. Graficar", 14);
                    colorT("6. Desplegar", 14);
                    colorT("0. Regresar", 15);
                    opcionMenu = validarNum();;
                    limpiar();
                    switch(opcionMenu){
                        case 0:
                            limpiar();
                            goto retornoEstructuraDatos;
                            break;
                        case 1:
                            limpiar();
                            insertarNodoListaDobleOrdenada();
                            goto retornoMenuListaDoble;
                            break;
                        case 2:
                            limpiar();
                            buscarNodoListaDoble();
                            goto retornoMenuListaDoble;
                            break;
                        case 3:
                            limpiar();
                            modificarNodoListaDoble();
                            goto retornoMenuListaDoble;
                            break;
                        case 4:
                            limpiar();
                            eliminarNodoListaDoble();
                            goto retornoMenuListaDoble;
                            break;
                        case 5:
                            //Graficar
                            limpiar();
                            graficarListaDoble();
                            goto retornoMenuListaDoble;
                            break;
                        case 6:
                            limpiar();
                            desplegarNodoListaDoble();
                            goto retornoMenuListaDoble;
                            break;
                        default:
                            cout << "Opción no válida, intenta de nuevo" << endl;
                            goto retornoMenuListaDoble;
                    }
                    break;
                case 4:
                    retornoMenuListaCircular:
                    colorT("#####################################################", 10);
                    colorT("####   1.1 LISTA CIRCULAR SIMPLEMENTE ENLAZADA   ####", 10);
                    colorT("#####################################################", 10);
                    colorT("Escoge una opción: ", 11);
                    colorT("1. Insertar", 14);
                    colorT("2. Ver", 14);
                    colorT("3. Editar", 14);
                    colorT("4. Eliminar", 14);
                    colorT("5. Graficar", 14);
                    colorT("6. Desplegar", 14);
                    colorT("0. Regresar", 15);
                    opcionMenu = validarNum();;
                    limpiar();
                    switch(opcionMenu){
                        case 0:
                            limpiar();
                            goto retornoEstructuraDatos;
                            break;
                        case 1:
                            limpiar();
                            insertarNodoListaCircularOrdenada();
                            goto retornoMenuListaCircular;
                            break;
                        case 2:
                            limpiar();
                            buscarNodoListaCircular();
                            goto retornoMenuListaCircular;
                            break;
                        case 3:
                            limpiar();
                            modificarNodoListaCircular();
                            goto retornoMenuListaCircular;
                            break;
                        case 4:
                            limpiar();
                            eliminarNodoListaCircular();
                            goto retornoMenuListaCircular;
                            break;
                        case 5:
                            //Graficar
                            limpiar();
                            graficarListaCircular();
                            goto retornoMenuListaCircular;
                            break;
                        case 6:
                            limpiar();
                            desplegarNodoListaCircular();
                            goto retornoMenuListaCircular;
                            break;
                        default:
                            cout << "Opción no válida, intenta de nuevo" << endl;
                            goto retornoMenuListaCircular;
                    }
                    break;
                case 0:
                    limpiar();
                    goto retornoMenuPrincipal;
                    break;
                default:
                    cout << "Opción no valida intenta de nuevo" << endl;
                    goto retornoEstructuraDatos;
            }
            break;
        case 2:
            retornoHojaDeCalculo:
            colorT("##################################", 10);
            colorT("##### 2.   HOJA DE CALCULO   #####", 10);
            colorT("##################################", 10);
            colorT("Escoge una opcion:", 11);
            colorT("1. Crear hoja", 14);
            colorT("2. Insertar", 14);
            colorT("3. Sumar", 14);
            colorT("4. Restar", 14);
            colorT("5. Multiplicar", 14);
            colorT("6. Graficar", 14);
            colorT("7. Deshacer", 14);
            colorT("8. Repetir", 14);
            colorT("0. Regresar", 15);
            opcionMenu = validarNum();;
            limpiar();

            switch(opcionMenu){
                case 0:
                    limpiar();
                    goto retornoMenuPrincipal;
                    break;
                case 1:
                    {
                        listaColumna = NULL;
                        listaFila = NULL;
                        pilaDeshacer = NULL;
                        pilaRepetir = NULL;
                        colorT("Ingresa el número de Filas (Y)", 15);
                        int iFila = validarNum();
                        colorT("Ingresa el número de columnas (X)", 15);
                        int iColumna = validarNum();
                        crearHoja(listaColumna, listaFila, iFila, iColumna);
                        goto retornoHojaDeCalculo;
                        break;
                    }
                case 2:
                    {
                        if(listaFila != NULL){
                            colorT("Ingresa la coordenada de la FILA:", 6);
                            int iFila = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA: ", 6);
                            int iColumna = validarNum();
                            colorT("Ingresa el valor a guardar: ", 14);
                            int valor = validarNum();
                            modificar(listaFila, iFila, iColumna, valor);
                            insertarPilaDeshacer(pilaDeshacer, iFila, iColumna, valor);
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                        goto retornoHojaDeCalculo;
                        break;
                    }
                case 3:
                    {
                        if(listaFila != NULL){
                            colorT("Ingresa la coordenada de la FILA:", 6);
                            int iFila = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA:", 6);
                            int iColumna = validarNum();
                            colorT("Ingresa la coordenada de la FILA:", 6);
                            int iFila2 = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA:", 6);
                            int iColumna2 = validarNum();

                            colorT("<-- ¿Donde deseas almacenar el resultado? -->", 11);
                            colorT("Ingresa la coordenada de la FILA: ", 10);
                            int iFilaResultado = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA: ", 10);
                            int iColumnaResultado = validarNum();
                            int busqueda = buscar(listaFila, iFila, iColumna);
                            int busqueda2 = buscar(listaFila, iFila2, iColumna2);
                            int suma = busqueda + busqueda2;
                            cout << "El resultado es: " << suma << endl;
                            modificar(listaFila, iFilaResultado, iColumnaResultado, suma);
                            insertarPilaDeshacer(pilaDeshacer, iFilaResultado, iColumnaResultado, suma);
                            cout << "El resultado se ha almacenado en la coordenada, FILA: " << iFilaResultado << " COLUMNA: " << iColumnaResultado << endl;
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                        goto retornoHojaDeCalculo;
                        break;
                    }
                case 4:
                    {
                        if(listaFila != NULL){
                            colorT("Ingresa la coordenada de la FILA:", 6);
                            int iFila = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA:", 6);
                            int iColumna = validarNum();
                            colorT("Ingresa la coordenada de la FILA:", 6);
                            int iFila2 = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA:", 6);
                            int iColumna2 = validarNum();

                            colorT("<-- ¿Donde deseas almacenar el resultado? -->", 11);
                            colorT("Ingresa la coordenada de la FILA: ", 10);
                            int iFilaResultado = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA: ", 10);
                            int iColumnaResultado = validarNum();
                            int busqueda = buscar(listaFila, iFila, iColumna);
                            int busqueda2 = buscar(listaFila, iFila2, iColumna2);
                            int resta = busqueda - busqueda2;
                            cout << "El resultado es: " << resta << endl;
                            modificar(listaFila, iFilaResultado, iColumnaResultado, resta);
                            insertarPilaDeshacer(pilaDeshacer, iFilaResultado, iColumnaResultado, resta);
                            cout << "El resultado se ha almacenado en la coordenada, FILA: " << iFilaResultado << " COLUMNA: " << iColumnaResultado << endl;
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                        goto retornoHojaDeCalculo;
                        break;
                    }
                    break;
                case 5:
                    {
                        if(listaFila != NULL){
                            colorT("Ingresa la coordenada de la FILA:", 6);
                            int iFila = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA:", 6);
                            int iColumna = validarNum();
                            colorT("Ingresa la coordenada de la FILA:", 6);
                            int iFila2 = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA:", 6);
                            int iColumna2 = validarNum();

                            colorT("<-- ¿Donde deseas almacenar el resultado? -->", 11);
                            colorT("Ingresa la coordenada de la FILA: ", 10);
                            int iFilaResultado = validarNum();
                            colorT("Ingresa la coordenada de la COLUMNA: ", 10);
                            int iColumnaResultado = validarNum();
                            int busqueda = buscar(listaFila, iFila, iColumna);
                            int busqueda2 = buscar(listaFila, iFila2, iColumna2);
                            int multiplicar = busqueda * busqueda2;
                            cout << "El resultado es: " << multiplicar << endl;
                            modificar(listaFila, iFilaResultado, iColumnaResultado, multiplicar);
                            insertarPilaDeshacer(pilaDeshacer, iFilaResultado, iColumnaResultado, multiplicar);
                            cout << "El resultado se ha almacenado en la coordenada, FILA: " << iFilaResultado << " COLUMNA: " << iColumnaResultado << endl;
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                        goto retornoHojaDeCalculo;
                        break;
                    }
                    break;
                case 6:
                    limpiar();
                    if(listaFila != NULL){
                          graficarHojaCalculo(listaFila);
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                    goto retornoHojaDeCalculo;
                    break;
                case 7:
                    if(listaFila != NULL){
                            eliminarPilaDeshacer(pilaDeshacer, pilaRepetir, listaFila);
                        colorT("Se ha deshecho la operación", 12);
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                    goto retornoHojaDeCalculo;
                    break;
                case 8:
                    if(listaFila != NULL){
                            eliminarPilaRepetir(pilaRepetir, pilaDeshacer, listaFila);
                        colorT("Se ha repetido la operación", 10);
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                    goto retornoHojaDeCalculo;
                case 20:
                    if(listaFila != NULL){
                         desplegarPilaDeshacer(pilaDeshacer);
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                    goto retornoHojaDeCalculo;
                    break;
                case 30:
                    if(listaFila != NULL){
                            desplegarPilaRepetir(pilaRepetir);
                        }else{
                            colorT("No hay hoja de calculo, crea una para continuar...", 12);
                        }

                    goto retornoHojaDeCalculo;
                default:
                    colorT("Opción no válida, intenta de nuevo", 13);
                    goto retornoHojaDeCalculo;
            }
            break;
        case 9:
            {
                colorT("No me quiero ir señor Stark :'v...", 14);
                int i = 0;
                while(i < 25){
                    colorT("Nooooooooooooooooooooooooooooo señor Stark!!!", 6);
                    i++;
                }
            return 0;
            }

        case 99:
            break;
        default:
            cout << "La opción no existe, intenta de nuevo" << endl;
            goto retornoMenuPrincipal;
    }
}

void insertarNodoPila(){
    nodoPila* nuevo = new nodoPila;
    colorT("Ingrese el ID del nuevo nodo", 7);
    nuevo->id = validarNum();
    colorT("Ingrese el VALOR del nuevo nodo", 7);
    cin >> nuevo->valor;
    nuevo->siguiente = primeroP;
    primeroP = nuevo;
    limpiar();
    colorT("Se ha insertado el nuevo nodo en la pila", 6);
}

void buscarNodoPila(){
    nodoPila* actual = new nodoPila;
    actual = primeroP;
    bool encontrado = false;
    int nodoBuscado = 0;

    colorT("Ingrese el ID del nodo a buscar en la pila", 15);
    nodoBuscado = validarNum();

    if(primeroP != NULL){
        while(actual != NULL){
            if(actual -> id == nodoBuscado){
                if(actual->siguiente == NULL){
                    cout << "ID: " << actual->id << endl << "VALOR: " << actual->valor << endl << endl << "Siguiente *: NULL" << endl << "Siguiente ID: NULL" << endl;
                }else{
                    cout << "ID: " << actual->id << endl << "VALOR: " << actual->valor << endl << endl<< "Siguiente *: " << actual->siguiente << endl << "Siguiente ID: " << actual->siguiente->id << endl;
                }
                cout << endl;
                encontrado = true;

            }
            actual = actual -> siguiente;
        }
        if(encontrado == 0){
            colorT("No se encontro el nodo en la pila", 6);
        }
    }else{
        colorT("La pila se encuentra vacia", 6);
    }
}

void modificarNodoPila(){
    nodoPila* actual = new nodoPila;
    actual = primeroP;
    int nodoBuscado = 0;
    bool encontrado = false;

    cout << "Ingrese el id del nodoPila a modificar" << endl;
    nodoBuscado = validarNum();
    if(primeroP != NULL){
        while(actual != NULL){
            if(actual -> id == nodoBuscado && encontrado != true){
                cout << "-->Informacion actual del nodoPila<---" << endl;
                cout << "ID: " << actual -> id << "\n VALOR: " << actual->valor << "\n" << endl;
                cout << "Ingrese el nuevo valor del nodoPila" << endl;
                cin >> &actual -> valor;
                colorT("El nodo de la pila fue modificado", 6);

                encontrado = true;
            }
            actual = actual -> siguiente;
        }
        if(encontrado == 0){

            printf("\n No se encontro el nodoPila \n");
        }
    }else{
        printf("\n Pila vacia \n");
    }
}

void desplegarPila(){
    nodoPila* actual = new nodoPila();
    actual = primeroP;
    if(actual != NULL){
        printf("La pila contiene: \n");
        while(actual != NULL){
            printf("id -> %d, valor -> %c, mem -> %p", actual -> id, actual -> valor,  actual);
            printf("\n");
            actual = actual -> siguiente;
        }
    }else{
        printf("\n Pila vacia \n");
    }
}

void graficarPila(){
    nodoPila* actual = new nodoPila();
    actual = primeroP;

    ofstream fout("pila.dot");
    int i = 0, i2 = 0;
    fout << "digraph g{ node [shape = box];" << endl;
    fout << "labelloc=\"t\";" << endl;
    fout << "label=\"PILA\";" << endl;

    if(actual != NULL){
        while(actual != NULL){
                fout << i << "[shape = record label = \"{ ID: "<< actual->id <<"\\n VALOR: " << actual->valor <<" | }\"];" << endl;
                //fout << i << "[label = \" ID: "<< actual->id <<"\\n VALOR: " << actual->valor <<"\"];" << endl;
            //printf("id -> %d, valor -> %c, mem -> %p", actual -> id, actual -> valor,  actual);
            //printf("\n");
            actual = actual -> siguiente;
            i++;
        }
        if((i-1) == 0){
            fout << 0 << "[style = filled, fillcolor = green];" <<endl;
        }
        if(i2+1 == i-1){
            fout << 1 << "[style = filled, fillcolor = red];" <<endl;
        }
        for (i2 = 0; i2 < i-1; i2++){
            if(i2 == 0){
                fout << i2 << "[style = filled, fillcolor = green];" <<endl;
            }else if(i2+1 == i-1){
                fout << i2+1 << "[style = filled, fillcolor = red];" <<endl;
            }
            fout << i2 << "->" << i2+1 << endl;
        }
    }else{
        printf("\n La pila se encuentra vacía, gráfica sera vacía \n");
    }
    fout << "}" << endl;
    fout.close();
    system("dot -Tpng pila.dot -o pila.png");
    colorT("La gráfica de la pila se ha generado n_n \n", 11);
    colorT("Abriendo gráfica de la PILA", 14);
    system("pila.png");
}

void graficarCola(){
    nodoCola* actual = new nodoCola();
    actual = primeroC;

    ofstream fout("cola.dot");
    int i = 0, i2 = 0;
    fout << "digraph g{ node [shape = box]; rankdir=\"LR\";" << endl;
    fout << "labelloc=\"t\";" << endl;
    fout << "label=\"COLA\";" << endl;

    if(primeroC != NULL){
        while(actual!=NULL){
                fout << i << "[shape = record label = \"{ ID: "<< actual->id <<"\\n VALOR: " << actual->valor <<" | }\"];" << endl;
                //fout << i << "[label = \" ID: "<< actual->id <<"\\n VALOR: " << actual->valor <<"\"];" << endl;
            //cout << "ID -> " << actual->id << "   VALOR -> " << actual->valor << "   MEM -> " << actual << endl;
            actual = actual ->siguiente;
            i++;
        }
        if((i-1) == 0){
            fout << 0 << "[style = filled, fillcolor = green];" <<endl;
        }
        if(i2+1 == i-1){
            fout << 1 << "[style = filled, fillcolor = red];" <<endl;
        }
        for (i2 = 0; i2 < i-1; i2++){
            if(i2 == 0){
                fout << i2 << "[style = filled, fillcolor = green];" <<endl;
            }else if(i2+1 == i-1){
                fout << i2+1 << "[style = filled, fillcolor = red];" <<endl;
            }
            fout << i2 << "->" << i2+1 << endl;
        }
    }else{
        printf("\n La cola se encuentra vacía, gráfica sera vacía \n");
    }
    fout << "}" << endl;
    fout.close();
    system("dot -Tpng cola.dot -o cola.png");
    colorT("La gráfica de la cola se ha generado n_n \n", 11);
    colorT("Abriendo gráfica de la COLA", 14);
    system("cola.png");
}

void graficarListaDoble(){
    nodoListaDoble* actual = new nodoListaDoble();
    actual = primerold;

    ofstream fout("listadoble.dot");
    int i = 0, i2 = 0;
    fout << "digraph g{ node [shape = box]; rankdir=\"LR\";" << endl;
    fout << "labelloc=\"t\";" << endl;
    fout << "label=\"LISTA DOBLEMENTE ENLAZADA\";" << endl;
    if(primerold != NULL){
        while(actual != NULL){
            fout << i << "[shape = record label = \"{ ID: "<< actual->id <<"\\n VALOR: " << actual->valor <<" | }\"];" << endl;
            //fout << i << "[label = \" ID: "<< actual->id <<"\\n VALOR: " << actual->valor <<"\"];" << endl;
            //cout << "ID: " << actual -> id << " VALOR: " << actual ->  valor << " MEM: " << actual << endl;
            actual = actual -> siguiente;
            i++;
        }
        if((i-1) == 0){
            fout << 0 << "[style = filled, fillcolor = green];" <<endl;
        }
        if(i2+1 == i-1){
            fout << 1 << "[style = filled, fillcolor = red];" <<endl;
        }
        for (i2 = 0; i2 < i-1; i2++){
            if(i2 == 0){
                fout << i2 << "[style = filled, fillcolor = green];" <<endl;
            }else if(i2+1 == i-1){
                fout << i2+1 << "[style = filled, fillcolor = red];" <<endl;
            }
            fout << i2 << "->" << i2+1 << endl;
            fout << i2+1 << "->" << i2 << endl;
        }
    }else{
        cout << "La lista esta vacia" << endl;
    }
    fout << "}" << endl;
    fout.close();
    system("dot -Tpng listadoble.dot -o listadoble.png");
    colorT("La gráfica de la lista doble se ha generado n_n \n", 11);
    colorT("Abriendo gráfica de la LISTA DOBLE ENLAZADA", 14);
    system("listadoble.png");
}

void graficarListaCircular(){
    nodoListaCircular * actual = new nodoListaCircular();
    actual = primerolc;

    ofstream fout("listacircular.dot");
    int i = 0, i2 = 0;
    fout << "digraph g{ node [shape = box]; rankdir=\"LR\";" << endl;
    fout << "labelloc=\"t\";" << endl;
    fout << "label=\"LISTA CIRCULAR SIMPLEMENTE ENLAZADA\";" << endl;
    if(primerolc != NULL){
        do{
            fout << i << "[shape = record label = \"{ ID: "<< actual->id <<"\\n VALOR: " << actual->valor <<" | }\"];" << endl;
            //fout << i << "[label = \" ID: "<< actual->id <<"\\n VALOR: " << actual->valor <<"\"];" << endl;
            //cout << "ID: " << actual->id << " VALOR: " << actual->valor << " MEM: " << actual << endl;
            actual = actual->siguiente;
            i++;
        }while(actual!=primerolc);
        if((i-1) == 0){
            fout << 0 << "[style = filled, fillcolor = green];" <<endl;
        }
        if(i2+1 == i-1){
            fout << 1 << "[style = filled, fillcolor = red];" <<endl;
        }
        for (i2 = 0; i2 < i-1; i2++){
            if(i2 == 0){
                fout << i2 << "[style = filled, fillcolor = green];" <<endl;
            }else if(i2+1 == i-1){
                fout << i2+1 << "[style = filled, fillcolor = red];" <<endl;
            }
            fout << i2 << "->" << i2+1 << endl;
        }
        fout << i2 << "->" << 0 << endl;
    }else{
        cout << "Lista circular vacia" << endl;
    }
    fout << "}" << endl;
    fout.close();
    system("dot -Tpng listacircular.dot -o listacircular.png");
    colorT("La gráfica de la lista circular se ha generado n_n \n", 11);
    colorT("Abriendo gráfica de la LISTA CIRCULAR", 14);
    system("listacircular.png");
}

void graficarHojaCalculo(pListaFila &lista){
    pListaFila auxLista = new (struct nodoFila);
    auxLista = lista;
    ofstream fout("hojacalculo.dot");
    fout << "digraph g{ node [shape = box]; rankdir=\"LR\";" << endl;
    fout << "labelloc=\"t\";" << endl;
    fout << "label=\"HOJA DE CALCULO\";" << endl;
    int f = 0, c = 0;
    char cv = 'a';
    while(auxLista != NULL){
        //cout << "Fila: " << auxLista->id << endl;
        if(auxLista->siguiente == NULL){
            fout << "f" << f << "[label=\"F: " << auxLista->id << "\"]" << endl;
        }else{
            fout << "f" << f << "[label=\"F: " << auxLista->id << "\"]" << endl;
            fout << "f" << f << "->f" << f+1 << endl;
        }
        pListaColumna aux = auxLista->columna;
        string a;
        while(aux != NULL){
                  //cout << "Columna: " << aux->id << " Valor: " << aux->valor << endl;
                  if(aux->siguiente == NULL){
                      fout << string(to_string(cv)) << c << "[label=\"C: " << aux->id<< "\\nV: " << aux->valor << "\"]" << endl;
                  }else{
                      fout << string(to_string(cv)) << c << "[label=\"C: " << aux->id<< "\\nV: " << aux->valor << "\"]" << endl;
                      fout << string(to_string(cv)) << c << "->" <<string(to_string(cv)) << c+1 <<endl;
                  }

                    a += " "+string(to_string(cv))+to_string(c);
                    aux = aux->siguiente;
                    c++;
        }
        fout << "{rank=same " << "f" << f << a << "}\n";
        fout << "f" << f << "->" << string(to_string(cv)) << 0 << endl;
        c=0;
        cv++;
        auxLista = auxLista->siguiente;
        f++;
    }
    fout << "}" << endl;
    fout.close();
    system("dot -Tpng hojacalculo.dot -o hojacalculo.png");
    colorT("La gráfica de la hoja de calculo se ha generado n_n \n", 11);
    colorT("Abriendo gráfica de la HOJA DE CALCULO", 14);
    system("hojacalculo.png");
}

void eliminarNodoPila(){
    nodoPila* actual = new nodoPila;
    nodoPila* anterior = new nodoPila();
    anterior = NULL;
    actual = primeroP;

    if(primeroP != NULL){
        if(actual == primeroP){
            primeroP = primeroP -> siguiente;
        }else{
            anterior -> siguiente = actual -> siguiente;
        }
        colorT("Nodo Eliminado de la pila!", 4);
        //esperar();
    }else{
        colorT("La pila se encuentra vacia", 6);
    }

    /*nodoPila* actual = new nodoPila;
    nodoPila* anterior = new nodoPila();
    anterior = NULL;
    actual = primeroP;
    int nodoBuscado = 0, encontrado = 0;

    cout << "Ingrese el ID del nodo a eliminar" << endl;
    cin >> nodoBuscado;

    if(primeroP != NULL){
        while(actual != NULL){
            if(actual -> id == nodoBuscado){

                if(actual == primeroP){
                    primeroP = primeroP -> siguiente;
                }else{
                    anterior -> siguiente = actual -> siguiente;
                }
                cout << "Nodo eliminado" << endl;
                encontrado = 1;
            }
            anterior = actual;
            actual = actual -> siguiente;
        }
        if(encontrado == 0){
            printf("\n No se encontro el nodoPila \n");
        }
    }else{
        printf("\n Pila vacia \n");
    }*/
}

void insertarNodoCola(){
    nodoCola* nuevo = new nodoCola();
    colorT("Ingrese el ID del nuevo nodo", 7);
    nuevo->id = validarNum();
    colorT("Ingrese el VALOR del nuevo nodo", 7);
    cin >> nuevo->valor;

    if(primeroC == NULL){
        primeroC = nuevo;
        primeroC ->siguiente = NULL;
        ultimoC = primeroC;
    }else{
        ultimoC -> siguiente = nuevo;
        nuevo -> siguiente = NULL;
        ultimoC = nuevo;
    }
    limpiar();
    colorT("Se ha insertado el nuevo nodo en la cola", 6);
}

void desplegarCola(){
    nodoCola* actual = new nodoCola();
    actual = primeroC;
    if(primeroC != NULL){
        while(actual!=NULL){
            cout << "ID -> " << actual->id << "   VALOR -> " << actual->valor << "   MEM -> " << actual << endl;
            actual = actual ->siguiente;
        }
    }else{
        cout << "Cola Vacia" << endl;
    }
}

void buscarNodoCola(){
    nodoCola* actual = new nodoCola();
    nodoCola* ultimo = new nodoCola();
    actual = primeroC;
    ultimo = ultimoC;

    bool encontrado = false;
    int nodoBuscado = 0;
    colorT("Ingrese el ID del nodo a buscar en la cola", 15);
    nodoBuscado = validarNum();
    if(primeroC != NULL){
        while(actual!=NULL){
            if(actual->id == nodoBuscado){
                if(actual -> siguiente != NULL){
                    cout << "ID: " << actual->id << endl << "VALOR: " << actual->valor << endl << endl << "Siguiente *: " << actual->siguiente << endl << "Siguiente ID: " << actual->siguiente->id << endl;
                }else{
                    cout << "ID: " << actual->id << endl << "VALOR: " << actual->valor << endl << endl << "Siguiente *: NULL" << endl << "Siguiente ID: NULL" << endl;
                }
                cout << endl;
                encontrado = true;
            }
            actual = actual ->siguiente;
        }
        if(encontrado == 0){
            colorT("No se encontro el nodo en la cola", 6);
        }
    }else{
        colorT("La cola se encuentra vacia", 6);
    }
}

void modificarNodoCola(){
    nodoCola* actual = new nodoCola();
    nodoCola* ultimo = new nodoCola();
    actual = primeroC;
    ultimo = ultimoC;

    bool encontrado = false;
    int nodoBuscado = 0;
    cout << "Ingresa el id del nodo a modicar" << endl;
    nodoBuscado = validarNum();
    if(primeroC != NULL){
        while(actual!=NULL && encontrado != true){
            if(actual->id == nodoBuscado){
                cout << "Ingrese el nuevo valor del nodo" << endl;
                cin >> actual -> valor;
                colorT("El nodo de la cola ha sido modificado", 6);
                encontrado = true;
            }
            actual = actual ->siguiente;
        }
    }else{
        cout << "Cola Vacia" << endl;
    }
}

void eliminarNodoCola(){
    nodoCola* actual = new nodoCola();
    actual = primeroC;
    nodoCola* anterior = new nodoCola;
    anterior = NULL;

    if(primeroC != NULL){
            if(actual == primeroC){
                primeroC = primeroC ->siguiente;
            }else if(actual == ultimoC){
                anterior ->siguiente = NULL;
                ultimoC = anterior;
            }else{
                anterior ->siguiente = actual -> siguiente;
            }
        colorT("Nodo Eliminado de la cola!", 4);
        //esperar();
    }else{
        colorT("La cola se encuentra vacia", 6);
    }

    /*nodoCola* actual = new nodoCola();
    actual = primeroC;
    nodoCola* anterior = new nodoCola;
    anterior = NULL;

    bool encontrado = false;
    int nodoBuscado = 0;
    cout << "Ingrese el ID del nodo a eliminar" << endl;
    cin >> nodoBuscado;
    if(primeroC != NULL){
        while(actual!=NULL && encontrado != true){
            if(actual->id == nodoBuscado){
                if(actual == primeroC){
                    primeroC = primeroC ->siguiente;
                }else if(actual == ultimoC){
                    anterior ->siguiente = NULL;
                    ultimoC = anterior;
                }else{
                    anterior ->siguiente = actual -> siguiente;
                }
                cout << "El nodo ha sido eliminado" << endl;

                encontrado = true;
            }
            anterior = actual;
            actual = actual ->siguiente;
        }
        if(!encontrado) cout << "Nodo no encontrado";
    }else{
        cout << "Cola Vacia" << endl;
    }*/
}

void insertarNodoListaDoble(){
    nodoListaDoble* nuevo = new nodoListaDoble();
    cout << "Inserte ID del nuevo nodo" << endl;
    nuevo->id = validarNum();
    cout << "Inserte Valor del nuevo nodo" << endl;
    cin >> nuevo->valor;
    if(primerold == NULL){
        primerold = nuevo;
        primerold -> siguiente = NULL;
        primerold -> atras = NULL;
        ultimold = primerold;
    }else{
        ultimold -> siguiente = nuevo;
        nuevo -> siguiente = NULL;
        nuevo -> atras = ultimold;
        ultimold = nuevo;
    }
    cout << "El nodo ha sido ingresado en la Lista Doble" << endl;
}

void insertarNodoListaDobleOrdenada(){
    nodoListaDoble* nuevo = new nodoListaDoble();
    nodoListaDoble* nodo = new nodoListaDoble();
    nodo = primerold;
    cout << "Inserte ID del nuevo nodo" << endl;
    nuevo->id = validarNum();
    cout << "Inserte Valor del nuevo nodo" << endl;
    cin >> nuevo->valor;


    if(primerold == NULL){
        primerold = nuevo;
        primerold -> siguiente = NULL;
        primerold -> atras = NULL;
        ultimold = primerold;
    }else{
        ultimold -> siguiente = nuevo;
        nuevo -> siguiente = NULL;
        nuevo -> atras = ultimold;
        ultimold = nuevo;


    while(nodo != NULL){
            nodoListaDoble* actual = nodo->siguiente;
            while(actual != NULL){
                if(nodo->id > actual->id){
                    int aux = actual->id;
                    char aux2 = actual->valor;
                    actual->id = nodo->id;
                    actual->valor = nodo->valor;
                    nodo->id = aux;
                    nodo->valor = aux2;
                }
                actual = actual->siguiente;
            }
            nodo = nodo->siguiente;
        }
    }
    cout << "El nodo ha sido ingresado en la lista doble" << endl;
}

void desplegarNodoListaDoble(){
    nodoListaDoble* actual = new nodoListaDoble();
    actual = primerold;
    if(primerold != NULL){
        while(actual != NULL){
            cout << "ID: " << actual -> id << " VALOR: " << actual ->  valor << " MEM: " << actual << endl;
            actual = actual -> siguiente;
        }
    }else{
        cout << "La lista esta vacia" << endl;
    }
}

void buscarNodoListaDoble(){
    nodoListaDoble* actual = new nodoListaDoble();
    actual = primerold;
    bool encontrado = false;
    int nodoBuscado = 0;
    cout << "Inserta el ID del nodo a buscar" << endl;
    nodoBuscado = validarNum();

    if(primerold != NULL){
        while(actual != NULL){
            if(actual -> id == nodoBuscado){
                cout << "ID: " << actual -> id << "\nVALOR: " << actual ->  valor << "\nMEM: " << actual << endl;
                if(actual->siguiente == NULL){
                    cout << "Siguiente *: NULL" << endl;
                    cout << "Siguiente ID: NULL" << endl;
                }else{
                    cout << "Siguiente *: " << actual->siguiente << endl;
                    cout << "Siguiente ID: " << actual->siguiente->id << endl;
                }

                if(actual->atras == NULL){
                    cout << "Anterior *: NULL" << endl;
                    cout << "Anterior ID: NULL" << endl;
                }else{
                    cout << "Anterior *: " << actual->atras << endl;
                    cout << "Anterior ID: " << actual->atras->id << endl;
                }

                encontrado = true;
            }

            actual = actual -> siguiente;
        }
        if(!encontrado){
            cout << "Nodo no encontrado" << endl;
        }
    }else{
        cout << "La lista esta vacia" << endl;
    }
}

void modificarNodoListaDoble(){
    nodoListaDoble* actual = new nodoListaDoble();
    actual = primerold;
    bool encontrado = false;
    int nodoBuscado = 0;
    cout << "Inserta el ID del nodo a modificar" << endl;
    nodoBuscado = validarNum();

    if(primerold != NULL){
        while(actual != NULL && encontrado != true){
            if(actual -> id == nodoBuscado){
                cout << "Ingrese el nuevo valor del nodo" << endl;
                cin >> actual -> valor ;
                cout << "El nodo ha sido modificado." << endl;
                encontrado = true;
            }

            actual = actual -> siguiente;
        }
        if(!encontrado){
            cout << "Nodo no encontrado" << endl;
        }
    }else{
        cout << "La lista esta vacia" << endl;
    }
}

void eliminarNodoListaDoble(){
    nodoListaDoble* actual = new nodoListaDoble();
    actual = primerold;
    nodoListaDoble* anterior = new nodoListaDoble();
    anterior = NULL;
    bool encontrado = false;
    int nodoBuscado = 0;
    cout << "Inserta el ID del nodo a eliminar" << endl;
    nodoBuscado = validarNum();
    if(primerold != NULL){
        while(actual != NULL && encontrado != true){
            if(actual -> id == nodoBuscado){
                    if(actual->siguiente == NULL && actual->atras == NULL){
                        primerold->siguiente = NULL;
                        primerold = NULL;
                    }else if (actual==primerold){
                            cout << "entro 1";
                            primerold = primerold -> siguiente;
                            primerold ->atras = NULL;
                    }else if(actual == ultimold){
                        cout << "entro 2";
                        anterior->siguiente = NULL;
                        ultimold = anterior;
                    }else{
                        cout << "entro 3";
                        anterior -> siguiente = actual -> siguiente;
                        actual->siguiente->atras = anterior;
                    }
                cout << "Nodo eliminado" << endl;
                encontrado = true;
            }
            anterior = actual;
            actual = actual -> siguiente;
        }
        if(!encontrado){
            cout << "Nodo no encontrado" << endl;
        }
    }else{
        cout << "La lista esta vacia" << endl;
    }
}

void insertarNodoListaCircular(){
    nodoListaCircular* nuevo = new nodoListaCircular();
    cout << "Ingrese el id del nuevo nodo" << endl;
    nuevo->id = validarNum();
    cout << "Ingrese el valor del nuevo nodo" << endl;
    cin >> nuevo->valor;

    if(primerolc == NULL){
        primerolc = nuevo;
        primerolc->siguiente = primerolc;
        ultimolc = primerolc;
    }else{
        ultimolc->siguiente = nuevo;
        nuevo->siguiente = primerolc;
        ultimolc = nuevo;
    }
    cout << "Nodo ingresado" << endl;
}

void insertarNodoListaCircularOrdenada(){
    nodoListaCircular* nuevo = new nodoListaCircular();
    nodoListaCircular* nodo = new nodoListaCircular();
    nodo = primerolc;
    cout << "Ingrese el id del nuevo nodo" << endl;
    nuevo->id = validarNum();
    cout << "Ingrese el valor del nuevo nodo" << endl;
    cin >> nuevo->valor;

    if(primerolc == NULL){
        primerolc = nuevo;
        primerolc->siguiente = primerolc;
        ultimolc = primerolc;
    }else{
        ultimolc->siguiente = nuevo;
        nuevo->siguiente = primerolc;
        ultimolc = nuevo;

        while(nodo->siguiente != primerolc){
            nodoListaCircular* actual = nodo->siguiente;
            while(actual != primerolc){
                if(nodo->id > actual->id){
                    int aux = actual->id;
                    char aux2 = actual->valor;
                    actual->id = nodo->id;
                    actual->valor = nodo->valor;
                    nodo->id = aux;
                    nodo->valor = aux2;
                }
                actual = actual->siguiente;
            }
            nodo = nodo->siguiente;
        }
    }
    cout << "Nodo ingresado" << endl;
}

void desplegarNodoListaCircular(){
    nodoListaCircular * actual = new nodoListaCircular();
    actual = primerolc;
    if(primerolc != NULL){
        do{
            cout << "ID: " << actual->id << " VALOR: " << actual->valor << " MEM: " << actual << endl;
            actual = actual->siguiente;
        }while(actual!=primerolc);
    }else{
        cout << "Lista circular vacia" << endl;
    }
}

void buscarNodoListaCircular(){
    nodoListaCircular* actual = new nodoListaCircular();
    actual = primerolc;
    bool encontrado = false;
    int nodoBuscado = 0;
    cout << "Ingrese el id del nodo a buscar" << endl;
    nodoBuscado = validarNum();

    if(primerolc != NULL){
        do{
            if(actual->id == nodoBuscado){
                cout << "ID: " << actual -> id << "\nVALOR: " << actual ->  valor << "\nMEM: " << actual << endl;
                cout << "Siguiente *: " << actual->siguiente << endl;
                cout << "Siguiente ID: " << actual->siguiente->id << endl;
                encontrado = true;
            }
            actual = actual -> siguiente;
        }while(actual != primerolc);
        if(!encontrado){
            cout << "Nodo no encontrado en la lista circular" << endl;
        }
    }else{
        cout << "La lista circular se encuentra vacia" << endl;
    }
}

void modificarNodoListaCircular(){
    nodoListaCircular* actual = new nodoListaCircular();
    actual = primerolc;
    bool encontrado = false;
    int nodoBuscado = 0;
    cout << "Ingrese el id del nodo a modificar" << endl;
    nodoBuscado = validarNum();

    if(primerolc != NULL){
        do{
            if(actual->id == nodoBuscado){
                cout << "Ingresa el nuevo valor del nodo a modificar" << endl;
                cin >> actual->valor;
                cout << "El nodo se ha modificado" << endl;
                encontrado = true;
            }
            actual = actual -> siguiente;
        }while(actual != primerolc && encontrado != true);
        if(!encontrado){
            cout << "Nodo no encontrado en la lista circular" << endl;
        }
    }else{
        cout << "La lista circular se encuentra vacia" << endl;
    }
}

void eliminarNodoListaCircular(){
    nodoListaCircular* actual = new nodoListaCircular();
    actual = primerolc;
    nodoListaCircular* anterior = new nodoListaCircular();
    anterior = NULL;
    bool encontrado = false;
    int nodoBuscado = 0;
    cout << "Ingrese el id del nodo a eliminar" << endl;
    nodoBuscado = validarNum();

    if(primerolc != NULL){
        do{
            if(actual->id == nodoBuscado){
                if(actual == (*actual).siguiente){
                    ultimolc -> siguiente = NULL;
                    primerolc -> siguiente = NULL;
                    primerolc = NULL;
                }else if(actual == primerolc){
                    primerolc = primerolc -> siguiente;
                    ultimolc -> siguiente = primerolc;
                }else if(actual == ultimolc) {
                    anterior->siguiente = primerolc;
                    ultimolc = anterior;
                }else{
                    anterior -> siguiente = actual -> siguiente;
                }
                cout << "Nodo eliminado de la lista circular" << endl;
                encontrado = true;
            }
            anterior = actual;
            actual = actual -> siguiente;
        }while(actual != primerolc && encontrado != true);
        if(!encontrado){
            cout << "Nodo no encontrado en la lista circular" << endl;
        }
    }else{
        cout << "La lista circular se encuentra vacia" << endl;
    }
}

void colorT(string texto, int color){
    HANDLE  hConsole;

    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    SetConsoleTextAttribute(hConsole, color);
    cout << texto << endl;
    SetConsoleTextAttribute(hConsole, 7);


    /*for(k = 1; k < 255; k++)
    {
        SetConsoleTextAttribute(hConsole, k);
        cout << k << " I want to be nice today!" << endl;
    }*/
}

void limpiar(){
    system("cls");
}

void esperar(){
    system("pause");
}

int validarNum(){
    int entrada;
    cin >> entrada;
    while (!cin.good()){
        cin.clear();
        cin.ignore(INT_MAX, '\n');
        colorT("El caracter no es númerico, intenta de nuevo...", 4);
        cin >> entrada;
    }
    return entrada;
}

void crearArchivo(){
 ofstream fs("test.txt");
 fs << "Hola, mundo" << endl;
 fs.close();
}
